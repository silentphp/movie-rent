<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\Entity\Movie;
use Application\Entity\Rental;
use Application\Service\CustomerService;
use Doctrine\ORM\EntityManager;
use Zend\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    private $em;

    private $customerService;

    public function __construct(EntityManager $em, CustomerService $customerService)
    {
        $this->em = $em;
        $this->customerService = $customerService;
    }

    public function indexAction()
    {

        $this->customerService->statement();

        // return new ViewModel();
    }


}
