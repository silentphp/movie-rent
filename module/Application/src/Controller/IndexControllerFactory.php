<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 13.05.18
 * Time: 8:18
 */

namespace Application\Controller;


use Application\Service\CustomerService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Entity;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class IndexControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get(EntityManager::class);
        $customerService = $container->get(CustomerService::class);
        return new IndexController($em, $customerService);
    }

}