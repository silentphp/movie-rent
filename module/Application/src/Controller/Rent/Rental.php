<?php


namespace Application\Controller\Rent;


class Rental
{

    private $movie;

    private $daysRented;

    function __construct(Movie $movie, int $daysRented)
    {
        $this->movie = $movie;
        $this->daysRented = $daysRented;
    }

    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * @return int
     */
    public function getDaysRented(): int
    {
        return $this->daysRented;
    }

    public function getCharge()
    {
        return $this->getMovie()->getCharge($this->getDaysRented());

    }

    /**
     * @return mixed
     */
    public function getFrequentRenterPoints()
    {
        if (($this->getMovie()->getPriceCode() == Movie::NEW_RELEASE) && $this->getDaysRented() > 1) {
            return 2;
        } else {
            return 1;
        }
    }

}