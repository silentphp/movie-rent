<?php


namespace Application\Controller\Rent;


class Movie
{
    const CHILDRENS = 2;
    const REGULAR = 0;
    const NEW_RELEASE = 1;

    private $title;

    private $priceCode;

    /** @var  Price */
    private $price;

    public function __construct(string $title, int $priceCode)
    {
        $this->title = $title;
        //$this->priceCode = $priceCode;
        $this->priceCode = $this->setPriceCode($priceCode);
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getPriceCode()
    {
        return $this->price->getPriceCode();
    }

    public function setPriceCode($priceCode): Void
    {
        switch ($priceCode) {
            case Movie::REGULAR:
                $this->price = new RegularPrice();
                break;
            case Movie::CHILDRENS:
                $this->price = new ChildrensPrice();
                break;
            case Movie::NEW_RELEASE:
                $this->price = new NewReleasePrice();
                break;
        }

    }

    public function getCharge($daysRented)
    {
        return $this->price->getCharge($daysRented);
        /*$result = 0;
        switch ($this->getPriceCode()) {
            case Movie::REGULAR:
                $result += 2;
                if ($daysRented > 2) {
                    $result += ($daysRented - 2) * 1.5;
                }
                break;
            case Movie::NEW_RELEASE:
                $result += $daysRented * 3;
                break;
            case Movie::CHILDRENS:
                $result += 1.5;
                if ($daysRented > 3) {
                    $result += ($daysRented - 3) * 1.5;
                }
                break;
        }
        return $result;*/
    }

}