<?php


namespace Application\Controller\Rent;


class ChildrensPrice extends Price
{
    public function getCharge($daysRented)
    {
        $result = 1.5;
        if ($daysRented > 3) {
            $result += ($daysRented - 3) * 1.5;
        }
        return $result;
    }

    public function getPriceCode()
    {
        return Movie::CHILDRENS;
    }
}