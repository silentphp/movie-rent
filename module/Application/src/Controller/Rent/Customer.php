<?php

namespace Application\Controller\Rent;

class Customer
{

    private $name;

    private $rentals = [];

    function __construct(string $name)
    {
        $this->name = $name;
    }

    public function addRental(Rental $rental)
    {
        $this->rentals[] = $rental;
    }

    public function getRental()
    {
        return $this->rentals;
    }

    public function getName()
    {
        return $this->name;
    }

    public function statement()
    {
        $result = 'Учет аренды: ' . $this->getName() . '<br>';
        /** @var Rental[] $rentals */
        $rentals = $this->getRental();
        foreach ($rentals as $item) {
            $result .= $item->getMovie()->getTitle() . ': ' . $item->getDaysRented() . ' дн. - '. $item->getCharge() . '$<br>';
        }
        $result .= '<br>Сумма долга: ' . $this->getTotalAmount();
        $result .= '<br>Бонусы: ' . $this->getFrequentRenterTotalPoints();

        return $result;
    }

    private function getTotalAmount()
    {
        $result = 0;
        /** @var Rental[] $rentals */
        $rentals = $this->getRental();
        foreach ($rentals as $item) {
            $result += $item->getCharge();
        }
        return $result;
    }

    private function getFrequentRenterTotalPoints()
    {
        $result = 0;
        /** @var Rental[] $rentals */
        $rentals = $this->getRental();
        foreach ($rentals as $item) {
            $result += $item->getFrequentRenterPoints();
        }
        return $result;
    }
}