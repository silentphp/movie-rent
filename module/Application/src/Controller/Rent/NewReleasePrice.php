<?php


namespace Application\Controller\Rent;


class NewReleasePrice extends Price
{
    public function getCharge($daysRented)
    {
        return $daysRented * 3;
    }

    public function getPriceCode()
    {
        return Movie::NEW_RELEASE;
    }
}