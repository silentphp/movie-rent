<?php


namespace Application\Controller\Rent;


abstract class Price
{
     abstract public function getCharge($daysRented);
     abstract public function getPriceCode();
}