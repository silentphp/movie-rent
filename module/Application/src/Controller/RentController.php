<?php


namespace Application\Controller;


use Application\Controller\Rent\Customer;
use Application\Controller\Rent\Movie;
use Application\Controller\Rent\Rental;
use Zend\Mvc\Controller\AbstractActionController;

class RentController extends AbstractActionController
{
    /**
     * @return void|\Zend\View\Model\ViewModel
     */
    public function indexAction()
    {

        $movie1 = new Movie("Терминатор", 0);
        $movie2 = new Movie("Терминатор 2", 1);
        $movie3 = new Movie("Остров", 1);
        $movie4 = new Movie("Фиксик убийца", 2);

        $rental1 = new Rental($movie1, 4);
        $rental2 = new Rental($movie2, 2);
        $rental3 = new Rental($movie3, 2);
        $rental4 = new Rental($movie4, 4);

        $customer = new Customer("Толик");
        $customer->addRental($rental1);
        $customer->addRental($rental2);
        $customer->addRental($rental3);
        $customer->addRental($rental4);

        echo $customer->statement();
    }

}