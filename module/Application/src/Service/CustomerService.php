<?php

namespace Application\Service;


use Application\Entity\Rental;
use Doctrine\ORM\EntityManager;

class CustomerService
{
    private $em;

    private $rentalService;

    private $movieService;

    public function __construct(EntityManager $em, RentalService $rentalService, MovieService $movieService)
    {
        $this->em = $em;
        $this->rentalService = $rentalService;
        $this->movieService = $movieService;
    }

    public function statement()
    {
        /** @var Rental[] $rental */
        $rental = $this->em->getRepository(Rental::class)->findAll();
        foreach ($rental as $item) {

            echo 'Дней проката ' . $item->getDaysRented();
            echo '<br>';
            echo 'Название фильма ' . $item->getMovie()->getTitle();
            echo '<br>';
            echo 'Имя клиента ' . $item->getCustomer()->getName();
            echo '<br>';
            echo 'Код цены ' . $item->getMovie()->getPriceCode();
            echo '<br>';
            echo 'Цена проката ' . $this->movieService->getCharge($item);
            echo '<br>';
            echo '<br>';
        }

        echo 'Сумма задолженности: ' . $this->getTotalCharge();
        echo '<br>';
        echo 'Всего бонусов: ' . $this->getTotalFrequentRenterPoints();

    }

    private function getTotalCharge()
    {
        $result = 0;
        /** @var Rental[] $rental */
        $rental = $this->em->getRepository(Rental::class)->findAll();
        foreach ($rental as $item) {
            $result += $this->movieService->getCharge($item);
        }
        return $result;
    }

    private function getTotalFrequentRenterPoints()
    {
        $result = 0;
        /** @var Rental[] $rental */
        $rental = $this->em->getRepository(Rental::class)->findAll();
        foreach ($rental as $item) {
            $result += $this->movieService->getFrequentRenterPoints($item);
        }
        return $result;
    }

}