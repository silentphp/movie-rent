<?php


namespace Application\Service;


use Application\Entity\Movie;
use Application\Entity\Rental;

class MovieService
{
    public function getCharge(Rental $aRental)
    {
        $result = 0;
        switch ($aRental->getMovie()->getPriceCode()) {
            case Movie::REGULAR:
                $result += 2;
                if ($aRental->getDaysRented() > 2) {
                    $result += (double)($aRental->getDaysRented() - 2) * 1.5;
                }
                break;
            case Movie::NEW_RELEASE:
                $result += (double)$aRental->getDaysRented() * 3;
                break;
            case Movie::CHILDRENS:
                $result += 1.5;
                if ($aRental->getDaysRented() > 3) {
                    $result += (double)($aRental->getDaysRented() - 3) * 1.5;
                }
                break;
        }
        return $result;
    }

    /**
     * @param Rental $aRental
     * @return mixed
     */
    public function getFrequentRenterPoints(Rental $aRental)
    {
        if ($aRental->getMovie()->getPriceCode() == Movie::NEW_RELEASE && $aRental->getDaysRented() > 1) {
            return 2;
        } else {
            return 1;
        }
    }

}