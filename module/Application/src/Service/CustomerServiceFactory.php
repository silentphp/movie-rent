<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 13.05.18
 * Time: 22:33
 */

namespace Application\Service;


use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Zend\ServiceManager\Exception\ServiceNotCreatedException;
use Zend\ServiceManager\Exception\ServiceNotFoundException;
use Zend\ServiceManager\Factory\FactoryInterface;

class CustomerServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $em = $container->get(EntityManager::class);
        $rentalService = $container->get(RentalService::class);
        $movieService = $container->get(MovieService::class);
        return new CustomerService($em, $rentalService, $movieService);
    }

}