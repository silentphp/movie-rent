<?php
/**
 * Created by PhpStorm.
 * User: serv
 * Date: 13.05.18
 * Time: 8:47
 */

namespace Application\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Rental
 *
 * @ORM\Entity
 *
 */
class Rental
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="integer")
     */
    private $daysRented;

    /**
     * @var Movie
     *
     * @ORM\ManyToOne(targetEntity="Movie", inversedBy="rental")
     * @ORM\JoinColumn(name="movie_id", referencedColumnName="id")
     */
    protected $movie;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="rental")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     */
    protected $customer;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Rental
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDaysRented()
    {
        return $this->daysRented;
    }

    /**
     * @param string $daysRented
     * @return Rental
     */
    public function setDaysRented($daysRented)
    {
        $this->daysRented = $daysRented;
        return $this;
    }

    /**
     * @return Movie
     */
    public function getMovie()
    {
        return $this->movie;
    }

    /**
     * @param Movie $movie
     */
    public function setMovie($movie)
    {
        $this->movie = $movie;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }



}